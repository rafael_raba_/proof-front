export class Client {
  id: string;
  documentType: string;
  documentNumber: string;
  email: string;
  phone: string;
}
