import {Component, OnInit} from '@angular/core';
import {Client} from '../core/models/Client';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClientService} from '../core/services/client.service';
import { v4 as uuidv4 } from 'uuid';


@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  public client = new Client();
  form: FormGroup;
  public documentsType = [
    {
      type: 'CC',
      description: 'Cedula de Ciudadania'
    },
    {
      type: 'TI',
      description: 'Tarjeta de Identidad'
    },
    {
      type: 'CE',
      description: 'Cedula de Extranjeria'
    },
  ];


  constructor(private formBuilder: FormBuilder, private clientService: ClientService ) {
    this.buildForm();
  }

  ngOnInit() {}

  public buildForm() {
    this.form = this.formBuilder.group(
      {
        documentType: ['', [Validators.required ]],
        documentNumber: ['', [Validators.required ]],
        email: ['', [Validators.required]],
        phone: ['', [Validators.required, Validators.minLength(10)]],
      }
    );
  }

  save(event: Event) {
    event.preventDefault();

    this.client.id = uuidv4();
    this.client.documentNumber = this.form.get('documentNumber').value
    this.client.documentType = this.form.get('documentType').value
    this.client.email = this.form.get('email').value
    this.client.phone = this.form.get('phone').value
    this.clientService.save(this.client).subscribe(response => {
      console.log(response)
    });

  }



}
